/*
 * gps.c
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "gps.h"
#include "usart.h"
#include "rtc.h"
#include "gpio.h"

extern xQueueHandle xGpsQueue;
extern xQueueHandle xDataQueue;

void vGpsTask(void *pvParameters){
	(void)pvParameters;

	char *xGpsSentence;
	xGpsQueue = xQueueCreate(5,sizeof(char*));

	GPS_Struct xGpsData;
	xGpsData.id = GPS_DATA;

	hGpsStart();

	while(1){
		if( xQueueReceive(xGpsQueue, &xGpsSentence, 2000) != pdTRUE){
			while(1){
				hLedGPSToggle();
				vTaskDelay( 50 );
			}
		}

		char *ptr = xGpsSentence;
		char *tmp = strsep(&ptr, ",");
		if( !strcmp("$GPRMC", tmp) ){
			tmp = strsep(&ptr, ",");
			xGpsData.time = str2time(tmp);
			tmp = strsep(&ptr, ",");
			if(tmp[0] == 'A') xGpsData.fix = 1;
			else xGpsData.fix = 0;
			tmp = strsep(&ptr, ",");
			xGpsData.latitude = str2DM(tmp);
			tmp = strsep(&ptr, ",");
			if(tmp[0] == 'S') xGpsData.latitude *= -1;
			tmp = strsep(&ptr, ",");
			xGpsData.longitude = str2DM(tmp);
			tmp = strsep(&ptr, ",");
			if(tmp[0] == 'W') xGpsData.longitude *= -1;
			tmp = strsep(&ptr, ",");
			xGpsData.speed = str2val(tmp);
			tmp = strsep(&ptr, ",");
			xGpsData.course = str2val(tmp);
			tmp = strsep(&ptr, ",");
			uint32_t *d =  (uint32_t *) &xGpsData.year;
			*d = str2date(tmp);

			DATA_STATUS *ptr = &xGpsData.id;
			xQueueSend(xDataQueue, &ptr, 500);
			if(xRtcNoUpdated() && xGpsData.year > 2000){
				hRtcSetTime(xGpsData.time, xGpsData.day, xGpsData.month, xGpsData.year);
			}

			if(xGpsData.fix){
				hLedGPSOff();
				vTaskDelay( 50 );
				hLedGPSOn();
			}
			else{
				hLedGPSOn();
				vTaskDelay( 50 );
				hLedGPSOff();
			}
		}
		else{
			hLedGPSOn();
			vTaskDelay( 400 );
			hLedGPSOff();
		}
	}
}


uint32_t str2time(char *str){
	uint32_t time = 0;
	if(str[0] != '\0'){
		uint32_t t = strtol(str, NULL, 10);
		time = (t % 100) * 10;
		time += ((t / 100) % 100) * 600;
		time += (t / 10000) * 36000;
	}
	return time;
}

uint32_t str2date(char *str){
	uint32_t date = 0;
	if(str[0] != '\0'){
		uint32_t t = strtol(str, NULL, 10);
		date = (t / 10000)<<24;
		date |= ((t % 10000) / 100)<<16;
		uint8_t y = t % 100;
		if(y >= 80) y = 0;
		date |= y + 2000;
	}
	return date;
}

int32_t str2DM(char *str){
	int32_t coord = 0;
	if(str[0] != '\0'){
		uint32_t dec = strtol(str, NULL, 10);
		char *dot = strchr(str,'.') + 1;
		uint8_t pos = strlen(dot);
		uint32_t mul = 1;
		for( ; pos<5; pos++) mul *= 10;
		uint32_t frac = strtol(dot, NULL, 10);
		coord = dec*100000 + frac*mul;
	}
	return coord;
}

uint16_t str2val(char *str){
	uint16_t ret = 0;
	if(str[0] != '\0'){
		uint16_t dec = strtol(str, NULL, 10);
		char *dot = strchr(str, '.') + 1;
		uint8_t frac = dot[0] - '0';
		ret = dec * 10 + frac;
	}
	return ret;
}
