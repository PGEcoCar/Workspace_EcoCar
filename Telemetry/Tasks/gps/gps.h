/*
 * gps.h
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#ifndef TASKS_GPS_GPS_H_
#define TASKS_GPS_GPS_H_

#include <stdint.h>
#include "data.h"

#define GPS_BUFFLEN			128
#define GPS_BUFFERS			2

#pragma pack(1)
typedef struct{
	DATA_STATUS id;
	uint32_t time;
	uint16_t year;
	uint8_t month;
	uint8_t day;
	int32_t latitude;
	int32_t longitude;
	uint16_t speed;
	uint16_t course;
	uint8_t fix;
	uint8_t terminator;
}GPS_Struct;

uint32_t str2time(char*);
uint32_t str2date(char*);
int32_t str2DM(char*);
uint16_t str2val(char*);
void vGpsTask(void*);


#endif /* TASKS_GPS_GPS_H_ */
