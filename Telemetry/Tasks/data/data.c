/*
 * data.c
 *
 *  Created on: 2 paź 2016
 *      Author: Pawel
 */

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "data.h"
#include "gps.h"

extern xQueueHandle xDataQueue;
extern xQueueHandle xGsmDataQueue;
extern xQueueHandle xSdDataQueue;

void vDataTask(void *pvParameters){
	(void)pvParameters;

	DATA_STATUS *xDataStatus;
	xDataQueue = xQueueCreate(10, sizeof(DATA_STATUS*));

	GPS_Struct xGpsData;
	Engine_Struct xEngineData;

	uint8_t xSdReady = pdFALSE;
	uint8_t xGsmReady = pdFALSE;

	while(1){
		if(xQueueReceive(xDataQueue, &xDataStatus, 100) == pdTRUE){
			switch(*xDataStatus){
			case ENGINE_DATA:

				break;
			case GPS_DATA:
				xGpsData = *((GPS_Struct *) xDataStatus);
				xGpsData.terminator = '\r';
				DATA_STATUS *ptr = &xGpsData.id;
				if(xSdReady) xQueueSend(xSdDataQueue, &ptr, 0);
				if(xGsmReady) xQueueSend(xGsmDataQueue, &ptr, 0);
				break;
			case SD_START:
				xSdReady = pdTRUE;
				break;
			case SD_STOP:
				xSdReady = pdFALSE;
				break;
			case GSM_START:
				xGsmReady = pdTRUE;
				break;
			case GSM_STOP:
				xGsmReady = pdFALSE;
				break;
			}
		}
	}
}
