/*
 * data.h
 *
 *  Created on: 2 paź 2016
 *      Author: Pawel
 */

#ifndef TASKS_DATA_DATA_H_
#define TASKS_DATA_DATA_H_

#include <stdint.h>

#define ENGINE_DATA_LEN		sizeof(Engine_Struct)
//#define DATA_QUEUE_LEN		23


typedef enum{
	ENGINE_DATA,
	GPS_DATA,
	SD_STOP,
	SD_START,
	GSM_START,
	GSM_STOP
} DATA_STATUS;

#pragma pack(1)
typedef struct{
	DATA_STATUS id;
	uint16_t time;
	uint16_t motor_current;
	uint16_t in_voltage;
	uint16_t mileage;
	uint16_t rpm;
	uint8_t terminator;
} Engine_Struct;



void vDataTask(void *);

#endif /* TASKS_DATA_DATA_H_ */
