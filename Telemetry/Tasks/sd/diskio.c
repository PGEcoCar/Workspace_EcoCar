/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2014        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"
#include "spi.h"
#include "gpio.h"

/* Definitions for MMC/SDC command */
#define CMD0	(0x40+0)	/* GO_IDLE_STATE */
#define CMD1	(0x40+1)	/* SEND_OP_COND (MMC) */
#define ACMD41	(0xC0+41)	/* SEND_OP_COND (SDC) */
#define CMD8	(0x40+8)	/* SEND_IF_COND */
#define CMD9	(0x40+9)	/* SEND_CSD */
#define CMD10	(0x40+10)	/* SEND_CID */
#define CMD12	(0x40+12)	/* STOP_TRANSMISSION */
#define ACMD13	(0xC0+13)	/* SD_STATUS (SDC) */
#define CMD16	(0x40+16)	/* SET_BLOCKLEN */
#define CMD17	(0x40+17)	/* READ_SINGLE_BLOCK */
#define CMD18	(0x40+18)	/* READ_MULTIPLE_BLOCK */
#define CMD23	(0x40+23)	/* SET_BLOCK_COUNT (MMC) */
#define ACMD23	(0xC0+23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24	(0x40+24)	/* WRITE_BLOCK */
#define CMD25	(0x40+25)	/* WRITE_MULTIPLE_BLOCK */
#define CMD55	(0x40+55)	/* APP_CMD */
#define CMD58	(0x40+58)	/* READ_OCR */

/* Card-Select Controls  (Platform dependent) */
#define SELECT()        hCSLow()    		/* MMC CS = L */
#define DESELECT()      hCSHigh()      		/* MMC CS = H */

static volatile DSTATUS Stat = STA_NOINIT; 	/* Disk status */
static volatile DWORD Timer1, Timer2; 		/* 1000Hz decrement timers */
static BYTE CardType; 						/* Card type flags */

/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------------------------------------------------*/

static BYTE wait_ready(void) {
	BYTE res;

	Timer2 = 500; 							/* Wait for ready in timeout of 500ms */
	rcvr_spi();
	do res = rcvr_spi();
	while ((res != 0xFF) && Timer2);

	return res;
}

/*-----------------------------------------------------------------------*/
/* Deselect the card and release SPI bus                                 */
/*-----------------------------------------------------------------------*/

static void release_spi(void) {
	DESELECT();
	rcvr_spi();
}

/*-----------------------------------------------------------------------*/
/* Receive a data packet from MMC                                        */
/*-----------------------------------------------------------------------*/

static uint8_t rcvr_datablock(BYTE *buff,	/* Data buffer to store received data */
							  UINT btr) {	/* Byte count (must be multiple of 4) */
	BYTE token;

	Timer1 = 100;
	do { 									/* Wait for data packet in timeout of 100ms */
		token = rcvr_spi();
	} while ((token == 0xFF) && Timer1);
	if (token != 0xFE) return 0; 			/* If not valid data token, return with error */

	do { 									/* Receive the data block into buffer */
		rcvr_spi_m(buff++);
		rcvr_spi_m(buff++);
		rcvr_spi_m(buff++);
		rcvr_spi_m(buff++);
	} while (btr -= 4);

	rcvr_spi(); 							/* Discard CRC */
	rcvr_spi();

	return 1; 								/* Return with success */
}

static uint8_t xmit_datablock(const BYTE *buff, 	/* 512 byte data block to be transmitted */
									BYTE token) { 	/* Data/Stop token */
	BYTE resp;
	BYTE wc;

	if (wait_ready() != 0xFF) return 0;

	xmit_spi(token); 						/* transmit data token */
	if (token != 0xFD) { 					/* Is data token */
		wc = 0;
		do { 								/* transmit the 512 byte data block to MMC */
			xmit_spi(*buff++);
			xmit_spi(*buff++);
		} while (--wc);

		xmit_spi(0xFF); 					/* CRC (Dummy) */
		xmit_spi(0xFF);
		resp = rcvr_spi(); 					/* Receive data response */
		if((resp & 0x1F) != 0x05) return 0;	/* If not accepted, return with error */
	}
	return 1;
}


/*-----------------------------------------------------------------------*/
/* Send a command packet to MMC                                          */
/*-----------------------------------------------------------------------*/

static BYTE send_cmd(BYTE cmd, 				/* Command byte */
					 DWORD arg) { 			/* Argument */
	BYTE n, res;

	if (cmd & 0x80) { 						/* ACMD<n> is the command sequence of CMD55-CMD<n> */
		cmd &= 0x7F;
		res = send_cmd(CMD55, 0);
		if (res > 1)
			return res;
	}

	/* Select the card and wait for ready */
	DESELECT();
	SELECT();
	if (wait_ready() != 0xFF) return 0xFF;

	/* Send command packet */
	xmit_spi(cmd); 							/* Start + Command index */
	xmit_spi((BYTE)(arg >> 24)); 			/* Argument[31..24] */
	xmit_spi((BYTE)(arg >> 16)); 			/* Argument[23..16] */
	xmit_spi((BYTE)(arg >> 8)); 			/* Argument[15..8] */
	xmit_spi((BYTE ) arg);					/* Argument[7..0] */
	n = 0x01; 								/* Dummy CRC + Stop */
	if (cmd == CMD0) n = 0x95; 				/* Valid CRC for CMD0(0) */
	if (cmd == CMD8) n = 0x87; 				/* Valid CRC for CMD8(0x1AA) */
	xmit_spi(n);

	/* Receive command response */
	if (cmd == CMD12) rcvr_spi(); 			/* Skip a stuff byte when stop reading */

	n = 100;								/* Wait for a valid response in timeout of 10 attempts */
	do res = rcvr_spi();
	while ((res & 0x80) && --n);
	return res; 							/* Return with the response value */
}

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE pdrv ) {			/* Physical drive nmuber to identify the drive */
	if (pdrv) return STA_NOINIT; 			/* Supports only single drive */
	return Stat;
}

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE pdrv 								/* Physical drive nmuber to identify the drive */
) {
	BYTE n, cmd, ty, ocr[4];

	if (pdrv) return STA_NOINIT;								/* Supports only single drive */
	if (Stat & STA_NODISK) return Stat; 						/* No card in the socket */

	FCLK_SLOW();												//interface_speed(INTERFACE_SLOW);
	for (n = 10; n; n--) rcvr_spi(); 							/* 80 dummy clocks */

	ty = 0;
	if (send_cmd(CMD0, 0) == 1) { 								/* Enter Idle state */
		Timer1 = 1000;											/* Initialization timeout of 1000 milliseconds */
		if (send_cmd(CMD8, 0x1AA) == 1) { 						/* SDHC */
			for (n = 0; n < 4; n++) ocr[n] = rcvr_spi(); 		/* Get trailing return value of R7 response */
			if (ocr[2] == 0x01 && ocr[3] == 0xAA) { 			/* The card can work at VDD range of 2.7-3.6V */
				while (Timer1 && send_cmd(ACMD41, 1UL << 30)); 	/* Wait for leaving idle state (ACMD41 with HCS bit) */
				if (Timer1 && send_cmd(CMD58, 0) == 0) { 		/* Check CCS bit in the OCR */
					for (n = 0; n < 4; n++) ocr[n] = rcvr_spi();
					ty = (ocr[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;
				}
			}
		} else { 												/* SDSC or MMC */
			if (send_cmd(ACMD41, 0) <= 1) {
				ty = CT_SD1;
				cmd = ACMD41; 									/* SDSC */
			} else {
				ty = CT_MMC;
				cmd = CMD1; 									/* MMC */
			}
			while (Timer1 && send_cmd(cmd, 0)); 				/* Wait for leaving idle state */
			if (!Timer1 || send_cmd(CMD16, 512) != 0) ty = 0;	/* Set R/W block length to 512 */
		}
	}
	CardType = ty;
	release_spi();

	if (ty) { 													/* Initialization succeeded */
		Stat &= ~STA_NOINIT; 									/* Clear STA_NOINIT */
		FCLK_FAST();
	}
	return Stat;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(BYTE pdrv, 							/* Physical drive nmuber to identify the drive */
				  BYTE *buff, 							/* Data buffer to store read data */
				  DWORD sector, 						/* Sector address in LBA */
				  UINT count ) {						/* Number of sectors to read */

	if (pdrv || !count) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;

	if (!(CardType & CT_BLOCK)) sector *= 512; 			/* Convert to byte address if needed */


	if (count == 1) { 									/* Single block read */
		if (send_cmd(CMD17, sector) == 0) { 			/* READ_SINGLE_BLOCK */
			if (rcvr_datablock(buff, 512)) count = 0;
		}
	} else {											/* Multiple block read */
		if (send_cmd(CMD18, sector) == 0) { 			/* READ_MULTIPLE_BLOCK */
			do {
				if (!rcvr_datablock(buff, 512)) break;
				buff += 512;
			} while (--count);
			send_cmd(CMD12, 0); 						/* STOP_TRANSMISSION */
		}
	}
	release_spi();

	return count ? RES_ERROR : RES_OK;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write (BYTE pdrv, 						/* Physical drive nmuber to identify the drive */
					const BYTE *buff, 				/* Data to be written */
					DWORD sector, 					/* Sector address in LBA */
					UINT count ){					/* Number of sectors to write */

	if (pdrv || !count) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	if (Stat & STA_PROTECT) return RES_WRPRT;

	if (!(CardType & CT_BLOCK)) sector *= 512; 		/* Convert to byte address if needed */

	if (count == 1) { 								/* Single block write */
		if ((	send_cmd(CMD24, sector) == 0) 		/* WRITE_BLOCK */
				&& xmit_datablock(buff, 0xFE))
			count = 0;
	}
	else { 											/* Multiple block write */
		if (CardType & CT_SDC) send_cmd(ACMD23, count);
		if (send_cmd(CMD25, sector) == 0) { 		/* WRITE_MULTIPLE_BLOCK */
			do {
				if (!xmit_datablock(buff, 0xFC)) break;
				buff += 512;
			}while (--count);
			if (!xmit_datablock(0, 0xFD)) 			/* STOP_TRAN token */
				count = 1;
		}
	}
	release_spi();

	return count ? RES_ERROR : RES_OK;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (BYTE pdrv, 					/* Physical drive nmuber (0..) */
					BYTE ctrl, 					/* Control code */
					void *buff ){ 				/* Buffer to send/receive control data */
	DRESULT res;
	BYTE n, csd[16], *ptr = buff;
	WORD csize;

	if (pdrv) return RES_PARERR;
	res = RES_ERROR;

	if (ctrl == CTRL_POWER) {
		switch (*ptr) {
		case 0:									/* Sub control code == 0 (POWER_OFF) */
			res = RES_OK;
			break;
		case 1: 								/* Sub control code == 1 (POWER_ON) */
			res = RES_OK;
			break;
		case 2: 								/* Sub control code == 2 (POWER_GET) */
			res = RES_OK;
			break;
		default :
			res = RES_PARERR;
		}
	}
	else {
		if (Stat & STA_NOINIT) return RES_NOTRDY;

		switch (ctrl) {
		case CTRL_SYNC : 								/* Make sure that no pending write process */
			SELECT();
			if (wait_ready() == 0xFF)
				res = RES_OK;
			break;
		case GET_SECTOR_COUNT : 						/* Get number of sectors on the disk (DWORD) */
			if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {
				if ((csd[0] >> 6) == 1) { 				/* SDC version 2.00 */
					csize = csd[9] + ((WORD)csd[8] << 8) + 1;
					*(DWORD*)buff = (DWORD)csize << 10;
				} else { 								/* SDC version 1.XX or MMC*/
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
					csize = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
					*(DWORD*)buff = (DWORD)csize << (n - 9);
				}
				res = RES_OK;
			}
			break;
		case GET_SECTOR_SIZE : 							/* Get R/W sector size (WORD) */
			*(WORD*)buff = 512;
			res = RES_OK;
			break;
		case GET_BLOCK_SIZE : 							/* Get erase block size in unit of sector (DWORD) */
			if (CardType & CT_SD2) { 					/* SDC version 2.00 */
				if (send_cmd(ACMD13, 0) == 0) { 		/* Read SD status */
					rcvr_spi();
					if (rcvr_datablock(csd, 16)) { 		/* Read partial block */
						for (n = 64 - 16; n; n--) rcvr_spi(); /* Purge trailing data */
						*(DWORD*)buff = 16UL << (csd[10] >> 4);
						res = RES_OK;
					}
				}
			} else { /* SDC version 1.XX or MMC */
				if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) { /* Read CSD */
					if (CardType & CT_SD1) { /* SDC version 1.XX */
						*(DWORD*)buff = (((csd[10] & 63) << 1) + ((WORD)(csd[11] & 128) >> 7) + 1) << ((csd[13] >> 6) - 1);
					} else { /* MMC */
						*(DWORD*)buff = ((WORD)((csd[10] & 124) >> 2) + 1) * (((csd[11] & 3) << 3) + ((csd[11] & 224) >> 5) + 1);
					}
					res = RES_OK;
				}
			}
			break;
		case MMC_GET_TYPE : 							/* Get card type flags (1 byte) */
			*ptr = CardType;
			res = RES_OK;
			break;
		case MMC_GET_CSD : 								/* Receive CSD as a data block (16 bytes) */
			if (send_cmd(CMD9, 0) == 0 					/* READ_CSD */
					&& rcvr_datablock(ptr, 16))
				res = RES_OK;
			break;
		case MMC_GET_CID : 								/* Receive CID as a data block (16 bytes) */
			if (send_cmd(CMD10, 0) == 0 				/* READ_CID */
					&& rcvr_datablock(ptr, 16))
				res = RES_OK;
			break;
		case MMC_GET_OCR : 								/* Receive OCR as an R3 resp (4 bytes) */
			if (send_cmd(CMD58, 0) == 0) { 				/* READ_OCR */
				for (n = 4; n; n--) *ptr++ = rcvr_spi();
				res = RES_OK;
			}
			break;
		case MMC_GET_SDSTAT : 							/* Receive SD status as a data block (64 bytes) */
			if (send_cmd(ACMD13, 0) == 0) { 			/* SD_STATUS */
				rcvr_spi();
				if (rcvr_datablock(ptr, 64))
					res = RES_OK;
			}
			break;
		default:
			res = RES_PARERR;
		}
		release_spi();
	}
	return res;
}
#endif

/*-----------------------------------------------------------------------*/
/* Device Timer Interrupt Procedure  (Platform dependent)                */
/*-----------------------------------------------------------------------*/
/* This function must be called in period of 1ms                        */


void disk_timerproc(void) {
	BYTE n;

	n = Timer1; 		/* 1000Hz decrement timers */
	if (n) Timer1 = --n;
	n = Timer2;
	if (n) Timer2 = --n;
}

