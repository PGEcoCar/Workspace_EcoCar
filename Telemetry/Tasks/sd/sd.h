/*
 * sd.h
 *
 *  Created on: 30 maj 2016
 *      Author: Pawel
 */
#ifndef SD_H_
#define SD_H_

#include "ff.h"
#include <stdio.h>
#include "gps.h"

typedef enum {
	SD_NOT_MOUNT,
	SD_MOUNT,
	SD_WRITE,
	SD_FAULT
} SD_STATUS;

//! Fatfs object
FATFS FatFs;
//! File object
FIL fil1;
FIL fil2;

void vSdTask( void * );
//DWORD get_fattime(void);
void disk_timerproc(void);
SD_STATUS mount_card(void);
SD_STATUS open_files(char *);
SD_STATUS close_files(void);
SD_STATUS write_gps_data(char *,GPS_Struct *);


char* d2a(int , char*);
char* ud2a(int , char* );
char* ud2an(int, char* , uint8_t);
char* msec2hms(uint32_t, char*);

#endif /* SD_H_ */
