
#include <stdint.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"

#include "stm32f10x.h"
#include "gpio.h"

void vLedTask( void *pvParameters  )
{
	(void )pvParameters;

	while( 1 )
	{
		hLedSToggle();
		vTaskDelay( 1000 );
	}
}

