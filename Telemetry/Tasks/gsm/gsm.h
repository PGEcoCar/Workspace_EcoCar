/*
 * gsm.h
 *
 *  Created on: 27 wrz 2016
 *      Author: Pawel
 */

#ifndef TASKS_GSM_GSM_H_
#define TASKS_GSM_GSM_H_

#include "data.h"

#define GSM_BUFFLEN			64
#define GSM_BUFFERS			5
#define REG_RETRY			120

typedef enum{
	ATE0,
	XISP,
	CREG,
	CSQ,
	CGDCONT,
	XIIC,
	XIICA,
	TCPSETUP,
	TCPCLOSE,
}GSM_CMD;

typedef enum{
	UNKNOWN,
	RN,
	WR,
	OK,
	STARTUP,
	PBREADY,
	SETUP_FAIL,
	SETUP_OK,
	CLOSE_OK,
	LINK_CLOSED,
	SEND_OK,
	SEND_ERR,
}GSM_RESP;

typedef enum{
	GSM_INIT,
	GSM_SETUP,
	GSM_READY,
	GSM_CONNECTED,
	GSM_SEND,
	ERR_INIT,
	ERR_CREG,
	ERR_XISP,
	ERR_CGDCONT,
	ERR_XIIC,
	ERR_IP,
	ERR_TCPSETUP,
	ERR_TCPCLOSE,
}GSM_STATUS;

void vGsmTask(void *);
GSM_RESP vHandleResponse(char[]);
GSM_STATUS vHandleCommand(GSM_CMD);
GSM_STATUS vGsmInit(void);
GSM_STATUS vGsmSetup(void);
GSM_STATUS vGsmConnect(void);
GSM_STATUS vGsmDisconnect(void);
void vGsmSendPing(void);
void vGsmSendGsmData(DATA_STATUS *);

#endif /* TASKS_GSM_GSM_H_ */
