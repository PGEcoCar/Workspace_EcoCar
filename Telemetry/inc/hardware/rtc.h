/*
 * rtc.h
 *
 *  Created on: 3 wrz 2016
 *      Author: Pawel
 */

#ifndef INCLUDE_HARDWARE_RTC_H_
#define INCLUDE_HARDWARE_RTC_H_

#include "stm32f10x_rtc.h"

uint8_t xDay;
uint8_t xMonth;
uint16_t xYear;
uint8_t xRtcUpdated;

void hRTCInit(void);
void hRtcSetTime(uint32_t,uint8_t,uint8_t,uint16_t);
uint8_t hRtcGetH(void);
uint8_t hRtcGetM(void);
uint8_t hRtcGetS(void);
uint8_t hRtcGetMS(void);

static inline uint16_t xRtcGetYear(void){
	return xYear;
}
static inline uint8_t xRtcGetMonth(void){
	return xMonth;
}
static inline uint8_t xRtcGetDay(void){
	return xDay;
}

static inline uint32_t hRtcGetTime(void){
	return RTC_GetCounter();
}

static inline uint8_t xRtcNoUpdated(void){
	return xRtcUpdated;
}

#endif /* INCLUDE_HARDWARE_RTC_H_ */
