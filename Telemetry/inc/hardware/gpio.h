/*
 * gpio.h
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#ifndef INCLUDE_HARDWARE_GPIO_H_
#define INCLUDE_HARDWARE_GPIO_H_

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"


#define LEDS_PIN 		GPIO_Pin_8
#define LEDGSMS_PIN		GPIO_Pin_9
#define LEDS_PORT		GPIOB
#define LEDGSM_PIN		GPIO_Pin_13
#define LEDSD_PIN		GPIO_Pin_14
#define LEDGPS_PIN		GPIO_Pin_15
#define LED_PORT		GPIOC

#define GPS_PORT		GPIOA
#define GPS_TX			GPIO_Pin_2
#define GPS_RX			GPIO_Pin_3

#define SD_PORT			GPIOA
#define SD_CS_PIN		GPIO_Pin_4
#define SD_SCK_PIN		GPIO_Pin_5
#define SD_MISO_PIN		GPIO_Pin_6
#define SD_MOSI_PIN		GPIO_Pin_7
#define SD_CD_PORT		GPIOB
#define SD_CD_PIN		GPIO_Pin_0
#define SD_CD			(SD_CD_PORT->IDR & SD_CD_PIN)

#define GSM_PORT		GPIOB
#define GSM_TX			GPIO_Pin_10
#define GSM_RX			GPIO_Pin_11
#define GSM_RING		GPIO_Pin_12
#define GSM_RST			GPIO_Pin_1


#define RS_PORT			GPIOA
#define RS_TX			GPIO_Pin_9
#define RS_RX			GPIO_Pin_10

#define SW_PORT			GPIOB
#define SW1_PIN			GPIO_Pin_13
#define SW2_PIN			GPIO_Pin_14
#define SW3_PIN			GPIO_Pin_15
#define SW1				(SW_PORT->IDR & SW1_PIN)
#define SW2				(SW_PORT->IDR & SW2_PIN)
#define SW3				(SW_PORT->IDR & SW3_PIN)

void hGpioInit(void);


static inline void hCSHigh(void){
	SD_PORT->ODR |= SD_CS_PIN;
}

static inline void hCSLow(void){
	SD_PORT->ODR &= ~SD_CS_PIN;
}


static inline void hGsmRstHigh(void){
	GSM_PORT->ODR |= GSM_RST;
}

static inline void hGsmRstLow(void){
	GSM_PORT->ODR &= ~GSM_RST;
}

//! LEDS toggle
static inline void hLedSToggle(void){
	LEDS_PORT->ODR ^= LEDS_PIN;
}

//! SD led inlines
static inline void hLedSDToggle(void){
	LED_PORT->ODR ^= LEDSD_PIN;
}

static inline void hLedSDOn(void){
	LED_PORT->ODR |= LEDSD_PIN;
}

static inline void hLedSDOff(void){
	LED_PORT->ODR &= ~LEDSD_PIN;
}

//! GSM led inlines
static inline void hLedGsmSOn(void){
	LEDS_PORT->ODR |= LEDGSMS_PIN;
}

static inline void hLedGsmSOff(void){
	LEDS_PORT->ODR &= ~LEDGSMS_PIN;
}

static inline void hLedGsmSToggle(void){
	LEDS_PORT->ODR ^= LEDGSMS_PIN;
}

static inline void hLedGsmOn(void){
	LED_PORT->ODR |= LEDGSM_PIN;
}

static inline void hLedGsmOff(void){
	LED_PORT->ODR &= ~LEDGSM_PIN;
}

static inline void hLedGsmToggle(void){
	LED_PORT->ODR ^= LEDGSM_PIN;
}


//! GPS led inlines

static inline void hLedGPSOn(void){
	LED_PORT->ODR |= LEDGPS_PIN;
}

static inline void hLedGPSOff(void){
	LED_PORT->ODR &= ~LEDGPS_PIN;
}

static inline void hLedGPSToggle(void){
	LED_PORT->ODR ^= LEDGPS_PIN;
}

#endif /* INCLUDE_HARDWARE_GPIO_H_ */
