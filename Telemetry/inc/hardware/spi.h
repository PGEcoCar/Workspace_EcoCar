/*
 * spi.h
 *
 *  Created on: 1 wrz 2016
 *      Author: Pawel
 */

#ifndef INCLUDE_HARDWARE_SPI_H_
#define INCLUDE_HARDWARE_SPI_H_

#include "stm32f10x.h"

#define SD_SPI		SPI1

/* Spi speed select */
#define FCLK_SLOW() { SD_SPI->CR1 = (SD_SPI->CR1 & ~0x38) | 0x28; }	/* Set SCLK = PCLK / 64 */
#define FCLK_FAST() { SD_SPI->CR1 = (SD_SPI->CR1 & ~0x38) | 0x08; }	/* Set SCLK = PCLK / 2 */

void hSpiInit(void);

/*-----------------------------------------------------------------------*/
/* Transmit a byte to MMC via SPI  (Platform dependent)                  */
/*-----------------------------------------------------------------------*/

static inline uint8_t xmit_spi( uint8_t out ) {
	SD_SPI->DR = out;
	while ( !(SD_SPI->SR & SPI_SR_RXNE) );
	return SD_SPI->DR;
}

/*-----------------------------------------------------------------------*/
/* Receive a byte from MMC via SPI  (Platform dependent)                 */
/*-----------------------------------------------------------------------*/

static inline uint8_t rcvr_spi(void) {
	return xmit_spi(0xff);
}

/* Alternative macro to receive data fast */
#define rcvr_spi_m(dst)  *(dst)=xmit_spi(0xff)

#endif /* INCLUDE_HARDWARE_SPI_H_ */
