/*
 * usart.h
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#ifndef INCLUDE_HARDWARE_USART_H_
#define INCLUDE_HARDWARE_USART_H_

#include "stm32f10x.h"

#define GPS_USART			USART2
#define GPS_RXNE			(GPS_USART->SR & USART_SR_RXNE)
#define GPS_IDLE			(GPS_USART->SR & USART_SR_IDLE)
#define GPS_ORE				(GPS_USART->SR & USART_SR_ORE)
#define GPS_FE				(GPS_USART->SR & USART_SR_FE)
#define GPS_NE				(GPS_USART->SR & USART_SR_NE)

#define GSM_USART			USART3
#define GSM_RXNE			(GSM_USART->SR & USART_SR_RXNE)
#define GSM_IDLE			(GSM_USART->SR & USART_SR_IDLE)
#define GSM_ORE				(GSM_USART->SR & USART_SR_ORE)
#define GSM_FE				(GSM_USART->SR & USART_SR_FE)
#define GSM_NE				(GSM_USART->SR & USART_SR_NE)

#define RS_USART			USART1
#define RS_RXNE				(RS_USART->SR & USART_SR_RXNE)
#define RS_IDLE				(RS_USART->SR & USART_SR_IDLE)
#define RS_ORE				(RS_USART->SR & USART_SR_ORE)
#define RS_FE				(RS_USART->SR & USART_SR_FE)
#define RS_NE				(RS_USART->SR & USART_SR_NE)


void hUsartInit(void);
void hGsmSend(char [], uint8_t);
void hDmaChannel3Reset(void);
void hDmaChannel6Reset(void);

static inline void hGpsStart(void){
	GPS_USART->CR1 |= USART_CR1_RE;
}

static inline void hGsmStart(void){
	GSM_USART->CR1 |= USART_CR1_RE | USART_CR1_TE;
}


#endif /* INCLUDE_HARDWARE_USART_H_ */
