
/* Library includes. */
#include <stdio.h>
#include <string.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

/* tasks */
#include "gps.h"
#include "gsm.h"
#include "sd.h"
#include "data.h"

volatile unsigned long ulHighFrequencyTimerTicks;
/*-----------------------------------------------------------*/

extern void SystemInit(void);
extern void HardwareInit(void);
extern void vLedTask(void *pvParameters);

xQueueHandle xDataQueue;
xQueueHandle xGpsQueue;
xQueueHandle xGsmQueue;
xQueueHandle xGsmDataQueue;
xQueueHandle xSdDataQueue;

int main(void){
	/* System start. */
	SystemInit();
	HardwareInit();

	/* Create the tasks. */
	xTaskCreate(vLedTask, "led", configMINIMAL_STACK_SIZE,
			NULL, tskIDLE_PRIORITY +1, NULL);
	xTaskCreate(vGpsTask, "gps", configMINIMAL_STACK_SIZE*2,
			NULL, tskIDLE_PRIORITY +2, NULL);
	xTaskCreate(vSdTask, "sd", configMINIMAL_STACK_SIZE*4,
			NULL, tskIDLE_PRIORITY +3, NULL);
	xTaskCreate(vGsmTask, "gsm", configMINIMAL_STACK_SIZE*4,
				NULL, tskIDLE_PRIORITY +4, NULL);
	xTaskCreate(vDataTask, "data", configMINIMAL_STACK_SIZE,
				NULL, tskIDLE_PRIORITY +3, NULL);

	/* Start the scheduler. */
	vTaskStartScheduler();

	return 0;
}




void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed portCHAR *pcTaskName)
{
	for( ;; );
}
