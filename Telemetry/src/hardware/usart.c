/*
 * usart.h
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#include "FreeRTOS.h"
#include "queue.h"

#include "usart.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "gsm.h"
#include "gps.h"
#include "data.h"


extern xQueueHandle xGpsQueue;
char gps_buffer[GPS_BUFFERS][GPS_BUFFLEN];
uint8_t gps_i;

extern xQueueHandle xGsmQueue;
char gsm_buffer[GSM_BUFFERS][GSM_BUFFLEN];
uint8_t gsm_i;

extern xQueueHandle xDataQueue;
char rs_buffer[ENGINE_DATA_LEN];


void hUsartInit(void){
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 |
							RCC_APB1Periph_USART3, ENABLE);
	RCC_AHBPeriphClockCmd(  RCC_AHBPeriph_DMA1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	USART_InitTypeDef USART_InitStruct;

	//! GPS USART
	USART_InitStruct.USART_BaudRate = 115200;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx;
	USART_Init(GPS_USART, &USART_InitStruct);

	GPS_USART->CR3 |= USART_CR3_DMAR;
	DMA1_Channel6->CCR = 0;
	DMA1_Channel6->CNDTR = GPS_BUFFLEN;
	DMA1_Channel6->CPAR = (uint32_t) &GPS_USART->DR;
	DMA1_Channel6->CMAR = (uint32_t) gps_buffer;
	DMA1_Channel6->CCR |= DMA_CCR6_PL_0 | DMA_CCR6_MINC | DMA_CCR6_EN;

	USART_Cmd(GPS_USART, ENABLE);
	GPS_USART->CR1 |= USART_CR1_IDLEIE;
	GPS_USART->CR3 |= USART_CR3_EIE;
	GPS_USART->BRR = 0x138;//0xEA6;
	NVIC_SetPriority(USART2_IRQn, 6);
	NVIC_EnableIRQ (USART2_IRQn);


	//! GSM USART
	USART_InitStruct.USART_BaudRate = 115200;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(GSM_USART, &USART_InitStruct);

	GSM_USART->CR3 |=  USART_CR3_DMAT | USART_CR3_DMAR;
	DMA1_Channel2->CCR = 0;
	DMA1_Channel2->CPAR = (uint32_t) &GSM_USART->DR;
	DMA1_Channel2->CCR |= DMA_CCR2_DIR | DMA_CCR2_MINC;

	DMA1_Channel3->CCR = 0;
	DMA1_Channel3->CNDTR = GSM_BUFFLEN;
	DMA1_Channel3->CPAR = (uint32_t) &GSM_USART->DR;
	DMA1_Channel3->CMAR = (uint32_t) gsm_buffer;
	DMA1_Channel3->CCR |= DMA_CCR3_PL_1 | DMA_CCR3_MINC | DMA_CCR3_EN;

	USART_Cmd(GSM_USART, ENABLE);
	GSM_USART->CR1 |= USART_CR1_IDLEIE;
	GSM_USART->CR3 |= USART_CR3_EIE;
	GSM_USART->BRR = 0x271;

	NVIC_SetPriority(USART3_IRQn, 6);
	NVIC_EnableIRQ (USART3_IRQn);


	//! RS485 USART
	//! GPS USART
	/*USART_InitStruct.USART_BaudRate = 19200;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx;
	USART_Init(RS_USART, &USART_InitStruct);

	RS_USART->CR3 |= USART_CR3_DMAR;
	DMA1_Channel6->CCR = 0;
	DMA1_Channel6->CNDTR = GPS_BUFFLEN;
	DMA1_Channel6->CPAR = (uint32_t) &RS_USART->DR;
	DMA1_Channel6->CMAR = (uint32_t) gps_buffer;
	DMA1_Channel6->CCR |= DMA_CCR6_PL_0 | DMA_CCR6_MINC | DMA_CCR6_EN;

	USART_Cmd(RS_USART, ENABLE);
	RS_USART->CR1 |= USART_CR1_IDLEIE;
	RS_USART->CR3 |= USART_CR3_EIE;
	NVIC_SetPriority(USART1_IRQn, 6);
	NVIC_EnableIRQ (USART1_IRQn);*/
}



void USART2_IRQHandler(void){
	char c = 0;
	static signed portBASE_TYPE xTaskWoken = pdFALSE;
	if( GPS_IDLE ){
		c = GPS_USART->SR;
		c = GPS_USART->DR;

		char *ptr = gps_buffer[gps_i];
		hDmaChannel6Reset();
		xQueueSendFromISR(xGpsQueue, &ptr , &xTaskWoken );
	}
	if( GPS_ORE || GPS_FE || GPS_NE){
		c = GPS_USART->SR;
		c = GPS_USART->DR;
	}
}


void USART3_IRQHandler(void){
	char c = 0;
	static signed portBASE_TYPE xTaskWoken = pdFALSE;
	if( GSM_IDLE ){
		c = GSM_USART->SR;
		c = GSM_USART->DR;
		char *ptr = gsm_buffer[gsm_i];
		hDmaChannel3Reset();
		xQueueSendFromISR(xGsmQueue, &ptr , &xTaskWoken );
	}
	if( GSM_ORE || GSM_FE || GSM_NE){
		c = GSM_USART->SR;
		c = GSM_USART->DR;
	}
}


void hGsmSend(char command[], uint8_t length) {
	DMA1_Channel2->CCR &= ~DMA_CCR2_EN;
	DMA1_Channel2->CMAR = (uint32_t) command; //rsBuffer;
	DMA1_Channel2->CNDTR = length;
	DMA1->IFCR |= DMA_IFCR_CTCIF2 | DMA_IFCR_CHTIF2;
	DMA1_Channel2->CCR |= DMA_CCR2_EN;
}


void hDmaChannel6Reset(void){
	DMA1_Channel6->CCR &= ~DMA_CCR6_EN;

	uint8_t tmp = GPS_BUFFLEN - DMA1_Channel6->CNDTR;
	gps_buffer[gps_i][tmp] = 0;
	gps_i++;
	if(gps_i == GPS_BUFFERS) gps_i = 0;
	DMA1_Channel6->CMAR = (uint32_t) gps_buffer[gps_i];
	DMA1_Channel6->CNDTR = GPS_BUFFLEN;

	DMA1_Channel6->CCR |= DMA_CCR6_EN;
}

void hDmaChannel3Reset(void){
	DMA1_Channel3->CCR &= ~DMA_CCR3_EN;

	uint8_t tmp = GSM_BUFFLEN - DMA1_Channel3->CNDTR;
	gsm_buffer[gsm_i][tmp] = 0;
	gsm_i++;
	if(gsm_i == GSM_BUFFERS) gsm_i = 0;
	DMA1_Channel3->CMAR = (uint32_t) gsm_buffer[gsm_i];
	DMA1_Channel3->CNDTR = GSM_BUFFLEN;

	DMA1_Channel3->CCR |= DMA_CCR3_EN;
}


