/*
 * hardware.c
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#include "FreeRTOS.h"
#include "queue.h"

#include "gpio.h"
#include "usart.h"
#include "spi.h"
#include "gps.h"
//#include "gsm.h"
#include "rtc.h"




uint8_t gps_i = 0;
uint8_t gsm_i = 0;


void HardwareInit(void){
	NVIC_SetPriorityGrouping( 4 );
	hRTCInit();
	hGpioInit();
	hUsartInit();
	hSpiInit();
}

/*
void USART2_IRQHandler(void){
	char c = 0;
	static signed portBASE_TYPE xTaskWoken = pdFALSE;
	if( GPS_RXNE ){
		gps_buffer[gps_i] = GPS_USART->DR;
		if( gps_buffer[gps_i] == '\n' ){
			xQueueSendFromISR( xGpsQueue, gps_buffer , &xTaskWoken );
			gps_i = 0;
		}
		else gps_i++;
		if(gps_i == GPS_BUFFLEN) gps_i = 0;
	}
	if( GPS_ORE ){
		c = GPS_USART->SR;
		c = GPS_USART->DR;
	}
}*/

/*

void USART3_IRQHandler(void){
	char c = 0;
	static signed portBASE_TYPE xTaskWoken = pdFALSE;
	if( GSM_RXNE ){
		gsm_buffer[gsm_i] = GSM_USART->DR;
		if( gsm_buffer[gsm_i] == '\n' ){
			xQueueSendFromISR( xGsmQueue, gsm_buffer , &xTaskWoken );
			gsm_i = 0;
		}
		else gsm_i++;
		if(gsm_i == GSM_BUFFLEN) gsm_i = 0;
	}
	if( GSM_ORE ){
		c = GSM_USART->SR;
		c = GSM_USART->DR;
	}
}*/


