/*
 * rtc.c
 *
 *  Created on: 3 wrz 2016
 *      Author: Pawel
 */

#include "stm32f10x.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_rcc.h"

#include "rtc.h"



void hRTCInit(void){

	RCC->APB1ENR |= RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN;
	PWR->CR |= PWR_CR_DBP;
	RCC->BDCR |= RCC_BDCR_RTCEN | RCC_BDCR_RTCSEL;

	RTC_WaitForSynchro();
	RTC_WaitForLastTask();
	RTC_SetPrescaler(0x1869);
	RTC_WaitForLastTask();
	RTC_SetAlarm(0xD2EFF);
	RTC_WaitForLastTask();
	RTC_ITConfig(RTC_IT_ALR,ENABLE);
	RTC_WaitForLastTask();
	RTC_SetCounter(0);

	xDay = 1;
	xMonth = 1;
	xYear = 1980;
	xRtcUpdated = 1;

	NVIC_EnableIRQ(RTCAlarm_IRQn);
}


void hRtcSetTime(uint32_t t,uint8_t d,uint8_t m,uint16_t y){
	xRtcUpdated = 0;

	//t += 72000;
	//t %= 864000;
	//if( (t / 36000) < 2 ) d++;
	RTC_SetCounter(t);

	xDay = d;
	xMonth = m;
	xYear = y;
}



void RTCAlarm_IRQHandler(void){
	RTC_ClearITPendingBit(RTC_IT_ALR);
	RTC_WaitForLastTask();
	RTC_SetAlarm( 0 );
}


