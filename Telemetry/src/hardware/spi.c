/*
 * spi.c
 *
 *  Created on: 1 wrz 2016
 *      Author: Pawel
 */

#include "freeRTOS.h"
#include "task.h"

#include "spi.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"

void hSpiInit(void){
	SPI_InitTypeDef SPI_InitStructure;
	uint8_t dummyread;

	/* Enable SPI clock, SPI1: APB2, SPI2: APB1 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	/* SPI configuration */
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256; // 72000kHz/256=281kHz < 400kHz
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;

	SD_SPI->CR1 |= (1UL << 5) | (1UL << 4) | (1UL << 3);

	SPI_Init(SD_SPI, &SPI_InitStructure);
	SPI_CalculateCRC(SPI1, DISABLE);
	SPI_Cmd(SD_SPI, ENABLE);

	/* drain SPI */
	while ( !(SD_SPI->SR & SPI_SR_TXE) );
	dummyread = SPI_I2S_ReceiveData(SD_SPI);
}
