/*
 * gpio.c
 *
 *  Created on: 30 sie 2016
 *      Author: Pawel
 */

#include "gpio.h"
#include "stm32f10x_rcc.h"

//! GPIO init function
/*!
 * GPIOA PIN1	GPS PPS
 * GPIOA PIN2	USART2 TX GPS
 * GPIOA PIN3	USART2 RX GPS
 * GPIOA PIN4	SD CS
 * GPIOA PIN5	SD SCK
 * GPIOA PIN6	SD MISO
 * GPIOA PIN7	SD MOSI
 * GPIOA PIN9 	USART1 TX RS485
 * GPIOA PIN10 	USART1 RX RS485
 * GPIOB PIN0	SD CD
 * GPIOB PIN1	Output GSM reset
 * GPIOB PIN6 	Input switch 1
 * GPIOB PIN7 	Input switch 2
 * GPIOB PIN8	LEDC
 * GPIOB PIN9	LEDGPRS1
 * GPIOB PIN10	USART3 TX GSM
 * GPIOB PIN11	USART3 RX GSM
 */
void hGpioInit(void) {

	RCC_APB2PeriphClockCmd(
			RCC_APB2Periph_GPIOA |
			RCC_APB2Periph_GPIOB |
			RCC_APB2Periph_GPIOC, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;

	//! LEDS
	GPIO_InitStruct.GPIO_Pin = LEDS_PIN | LEDGSMS_PIN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(LEDS_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = LEDSD_PIN | LEDGSM_PIN | LEDGPS_PIN;
	GPIO_Init(LED_PORT, &GPIO_InitStruct);


	//! GPS
	GPIO_InitStruct.GPIO_Pin = GPS_TX;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPS_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = GPS_RX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPS_PORT, &GPIO_InitStruct);

	//! SD
	GPIO_InitStruct.GPIO_Pin = SD_CS_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(SD_PORT, &GPIO_InitStruct);
	SD_PORT->ODR |= SD_CS_PIN;

	GPIO_InitStruct.GPIO_Pin = SD_SCK_PIN | SD_MOSI_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(SD_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = SD_MISO_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(SD_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = SD_CD_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(SD_CD_PORT, &GPIO_InitStruct);

	//! RS485
	GPIO_InitStruct.GPIO_Pin = RS_TX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(RS_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = RS_RX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(RS_PORT, &GPIO_InitStruct);

	//! GSM
	GPIO_InitStruct.GPIO_Pin = GSM_TX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GSM_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = GSM_RX;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GSM_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = GSM_RST;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GSM_PORT, &GPIO_InitStruct);
	hGsmRstHigh();

	//! SW
	GPIO_InitStruct.GPIO_Pin = SW1_PIN | SW2_PIN | SW3_PIN;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(SW_PORT, &GPIO_InitStruct);
}
