
#define F_CPU 8000000UL		//taktowanie procesora
#define F_SCL 100000UL		//taktowanie magistrali i2c


#include <avr/io.h>
#include <stdint.h>                     // needed for uint8_t
#include <util/twi.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define SIGNAL				(1<<PD7)
#define LED_RED 			(1<<PB1)
#define LED_GREEN 			(1<<PB0)
#define BUZZER 				(1<<PB2)

#define LED_RED_ON 			PORTB |= LED_RED
#define LED_RED_OFF 		PORTB &= ~LED_RED

#define SIGNAL_HIGHT 		PORTD |=SIGNAL
#define SIGNAL_LOW	 		PORTD &= ~SIGNAL

#define LED_GREEN_ON 		PORTB |= LED_GREEN
#define LED_GREEN_OFF 		PORTB &= ~LED_GREEN

#define BUZZER_ON 			PORTB |= BUZZER
#define BUZZER_OFF 			PORTB &= ~BUZZER

#define TWACK 				TWCR=(1<<TWINT)|(1<<TWEN)|(1<<TWEA)
#define TWNACK 				TWCR=(1<<TWINT)|(1<<TWEN)
#define TWRESET 			TWCR=(1<<TWINT)|(1<<TWEN)|(1<<TWSTO)|(1<<TWEA)
#define TWSTART 			TWCR=(1<<TWINT)|(1<<TWEN)|(1<<TWSTA)
#define TWSTOP 				TWCR=(1<<TWINT)|(1<<TWEN)|(1<<TWSTO)

#define I2C_SLAVE_ADDRESS 0x01
#define SLA_W ((I2C_SLAVE_ADDRESS<<1) | TW_WRITE)

#define BUFLEN_TRAN 2



void i2c_send(uint16_t);
uint16_t pomiar (uint8_t kanal);					// deklaracja funkcji pomiaru


uint16_t adcValue;
uint16_t kanal;




int main( void )
	{
	SIGNAL_LOW;										//inicjalizacja stanu niskiego na SIGNAL

    ADCSRA |= (1<<ADEN)|(1<<ADSC);					// wlacz ADC

	ADMUX |= (1<<REFS0);							// napi�cie referencyjne 5 [V]

    ADCSRA |=(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);		// preskaler ustawiony na wartosc 128

	TWBR =( (F_CPU/F_SCL)-16 )/2;

	TWCR = (1<<TWEN)|(1<<TWEA)|(1<<TWIE);			//aktywacja i2c

	 DDRB |= LED_RED | LED_GREEN | BUZZER ; 			// kierunek wyjsciowy na pinach portu B
	   DDRD |= SIGNAL;									// kierunek wyjciowy na pinie portu D

		LED_RED_ON;										// wst�pny test element�w sygnalizacyjnych
		LED_GREEN_ON;

		int p;
		for (p=0; p<3; p++)
			{
			BUZZER_ON;
			_delay_ms(200);
			BUZZER_OFF;
			_delay_ms(200);
			}

				LED_RED_OFF;
				LED_GREEN_OFF;
				BUZZER_OFF;


		int w;								// 20-sekundowe wygrzanie sensora MQ-8
		for (w=0; w<20; w++)
			{
			LED_RED_ON, LED_GREEN_ON;
			_delay_ms(500);
			LED_RED_OFF, LED_GREEN_OFF;
			_delay_ms(500);

			if(w>17) BUZZER_ON;
			}

			LED_RED_OFF;					// wygaszenie element�w sygnalizacyjnych, koniec wygrzewania sensora MQ - 8
			LED_GREEN_OFF;
			BUZZER_OFF;
    sei();
    while(1){
    	adcValue = pomiar(1);					// odczytana wartosc z ADC
    	i2c_send(adcValue);						//wysyłanie 0x1234

    	if (adcValue > 350)					// warunek wyboru rodzaju sygnalizacji
    				{
    				SIGNAL_HIGHT;				// ustawienie stanu wysokiego na SIGNAL
    				LED_RED_ON;					// sygnalizacja czerwon� LED
    				_delay_ms(70);
    				LED_RED_OFF;

    				BUZZER_ON;					// sygnalizacja BUZZER
    				_delay_ms(100);
    				BUZZER_OFF;
    				_delay_ms(90);
    				}

    	if (adcValue < 350)
    			   {
    				SIGNAL_LOW;					// ustawienie stanu niskiego na SIGNAL
    				LED_GREEN_ON;				// sygnalizacja zielon� LED
    				_delay_ms(40);
    				LED_GREEN_OFF;
    				_delay_ms(400);
    			  }
    }
}



uint16_t pomiar (uint8_t kanal)

	{

	ADMUX |= (ADMUX & 0xF8) | kanal;

	ADCSRA |= (1<<ADSC);

	while  (ADCSRA & (1<<ADSC));

	return ADCW;

	}


void i2c_send(uint16_t adcValue){

	uint8_t out[BUFLEN_TRAN];				//tymczasowy buffor
	out[0] = adcValue;
	out[1] = adcValue>>8;

	TWSTART;
	while(!( TWCR & (1<<TWINT)) );
	TWDR = SLA_W;
	TWACK;
	while(!(TWCR & (1 << TWINT)));
	for(uint8_t i=0;i<BUFLEN_TRAN;i++){
		TWDR = out[ i ];
		TWACK;
		while(!(TWCR & (1<<TWINT)));
	}
	TWSTOP;
}




