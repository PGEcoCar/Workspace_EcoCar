#ifndef CONV_H_
#define CONV_H_

#include <stdio.h>

#define VIN_FACTOR 	0.009348
#define V_OFFSET 	2.5
#define VACS_FACTOR 0.000806
#define TR	21
#define WHEEL 2


float getVoltage(uint16_t vin);
float getCurrent(uint16_t cin);
float getVelocity(uint16_t rpm);

#endif
