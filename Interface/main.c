#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "lcd.h"
#include <avr/sleep.h>

//set desired baud rate
#define BAUDRATE 19200
//calculate UBRR value
#define UBRRVAL ((F_CPU/(BAUDRATE*16UL))-1)

typedef int bool;
#define true 1
#define false 0

typedef struct{
	float velocity;
	float av_velocity;
	float distance;
	int power;
	int rpm;
	float voltage_in;
	float current_in;
}converted_data;

typedef struct {
	char start;
	uint16_t time;
	uint32_t current_acc;
	uint32_t current_cnt;
	uint16_t current_mean; // to fuck out
	uint16_t limit;
	uint16_t in_voltage;
	uint32_t mileage;
	int16_t rpm;
	uint8_t sw;
	char stop;
} data_struct;

#define OUT_BUFF_LEN sizeof(data_struct)
volatile uint8_t in_buffer[OUT_BUFF_LEN];
data_struct *data = (data_struct *) in_buffer;
volatile bool isReceiving = false;
volatile bool updateBitch=false;

volatile int count = 0;
char buf[16];

void USART_Init() {
	//Set baud rate
	UBRRL = UBRRVAL;		//low byte
	UBRRH = (UBRRVAL >> 8);	//high byte
	//Set data frame format: asynchronous mode,no parity, 1 stop bit, 8 bit size
	UCSRC = (1 << URSEL) | (0 << UMSEL) | (0 << UPM1) | (0 << UPM0)
									| (0 << USBS) | (0 << UCSZ2) | (1 << UCSZ1) | (1 << UCSZ0);
	//Enable Transmitter and Receiver and Interrupt on receive complete
	UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
	//enable global interrupts
	set_sleep_mode(SLEEP_MODE_IDLE);
	sei();
}

ISR(USART_RXC_vect) {
	char Temp;
	Temp = UDR;
	//count1++;
	if((Temp == 'S') && (isReceiving == false)){
		count = 0;
		isReceiving = true;
		in_buffer[count] = Temp;
	}
	else if(isReceiving){
		count++;
		in_buffer[count] = Temp;
	}
	if( count == (OUT_BUFF_LEN - 1)){
		if((Temp == '\n') && (isReceiving == true)){
				updateBitch=true;
		}
		isReceiving = false;
		count = 0;
	}

}


int main(void) {
	USART_Init();
	lcd_init(LCD_DISP_ON);
	lcd_gotoxy(0, 0);
	lcd_puts("Eco Car");
	while (1) {
		while(!updateBitch);
		lcd_clrscr();
		lcd_gotoxy(0, 0);

		if((data->sw ) & 0x01) lcd_putc('0');
		else lcd_putc('1');
		lcd_puts("  ");

		uint16_t speed = data->rpm / 13;
		uint8_t d = speed % 10;
		uint8_t u = (speed / 100 ) % 10;
		uint8_t l = (speed / 10 ) % 10;
		//utoa(speed/10,buf,2);
		lcd_putc(u + '0');
		lcd_putc(l + '0');
		lcd_putc('.');
		lcd_putc(d+ '0');
		lcd_puts("  ");

		uint32_t dist = data->mileage * 8447;
		dist /= 10000;
		uint32_t time = data->time/10;

		uint32_t spd = dist * 1000;
		spd /= time;
		spd /= 360;

		d = spd % 10;
		u = (spd / 100 ) % 10;
		l = (spd / 10 ) % 10;
		//utoa(speed/10,buf,2);
		lcd_putc(u + '0');
		lcd_putc(l + '0');
		lcd_putc('.');
		lcd_putc(d+ '0');
		lcd_puts("  ");
		//utoa(rpm,buf,4);
		uint8_t a;// = rpm / 1000;
		//lcd_putc(a+'0');
		//a = (rpm / 100) % 10;
		//lcd_putc(a+'0');
		//a = (rpm / 10) % 10;
		//lcd_putc(a+'0');
		//a = rpm % 10;
		//lcd_putc(a+'0');
		if((data->sw >> 3) & 0x01) lcd_putc('0');
		else lcd_putc('1');
		//uint16_t m = data->current_mean;
		//utoa(m, buf, 10);
		//lcd_puts(buf);
		//lcd_puts(" L:");
		//utoa(data->limit, buf, 10);
		//lcd_puts(buf);

		//a[1] = '0' + (data->sw & 0x01 );
		//a[0] = '0' + ((data->sw >>1) & 0x01);
		//lcd_gotoxy(14, 0);
		//lcd_puts(a);

		lcd_gotoxy(0, 1);
		if((data->sw >> 2) & 0x01) lcd_putc('0');
		else lcd_putc('1');
		lcd_puts("  ");
		a = data->current_mean/1000;
		lcd_putc(a+'0');
		a = (data->current_mean / 100) % 10;
		lcd_putc(a+'0');
		a = (data->current_mean / 10) % 10;
		lcd_putc(a+'0');
		a = data->current_mean % 10;
		lcd_putc(a+'0');

		lcd_puts("  ");
		a = data->limit/1000;
		lcd_putc(a+'0');
		a = (data->limit / 100) % 10;
		lcd_putc(a+'0');
		a = (data->limit / 10) % 10;
		lcd_putc(a+'0');
		a = data->limit % 10;
		lcd_putc(a+'0');

		lcd_puts("  ");
		if((data->sw >> 1) & 0x01) lcd_putc('0');
		else lcd_putc('1');


		updateBitch=false;
	}

	return 0;
}
