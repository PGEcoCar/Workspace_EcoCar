#include "conv.h"
#include <stdio.h>

float getVoltage(uint16_t vin){
	return vin*VIN_FACTOR;
}
float getCurrent(uint16_t cin){
	return 15*(V_OFFSET-cin*VACS_FACTOR);
}
float getVelocity(uint16_t rpm){
	return (float)(WHEEL*rpm*3.6)/(float)(40*TR);
}
