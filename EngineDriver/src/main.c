
// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"

#include "gpio.h"
#include "timer.h"
#include "usart.h"
#include "adc.h"
#include "watchdog.h"

#include "type.h"
#include "usbreg.h"
#include "usbcore.h"
#include "usb.h"
#include "usbcfg.h"
#include "usbhw.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


//! Higl/Low transistor on
uint8_t HL = 0;
//! Surrent limit value
uint16_t limit = 0;
//! Data structure
data_struct *data = (data_struct *) out_buffer;
//! Switch status, defines in gpio.h
uint8_t sw_status = 0;


//! Calculated efficiency table
uint16_t efficiency_table[120];

int main(int argc, char* argv[]) {

	watchdog_init();
	sw_status = gpio_init();
	current_adc_init();
	slow_adc_init();
	pwm_timer_init();
	encoder_timer_init();
	usart3_init();
	SysTick_Config(SystemCoreClock / 10);

	if(sw_status & USB_ON){
		USB_Init();
		USB_Connect(TRUE);
	}
	if(sw_status & ENCODER_ON) {
		mileage_timer_init();
	}

	while (1) {
		average();
	}
}


//! Main timer, 10Hz interrupt
void SysTick_Handler(void) {
	data->time++;
	data->motor_current = acc/counter;
	limit = efficiency_table[ get_encoder_val( &data->rpm)/40 ];
	acc = counter = 0;

	usart3_send();
	start_slow_conversion( &data->voltage );

	//! Only for testability
	limit = data->voltage;

	if(GPIOA->IDR & GPIO_IDR_IDR4) pwm_off();
	else pwm_on();
	wdg_reset;
}


//! ADC1 and ADC2 interrupt handler
void ADC1_2_IRQHandler() {
	if(ADC1->SR & ADC_SR_AWD) {
		if(HL) {
			set_high();
			set_lower_limit(limit);
			HL=0;
		}
		else {
			set_low();
			set_upper_limit(limit);
			HL=1;
		}
		ADC1->SR &= ~ADC_SR_AWD;
	}
	if(ADC2->SR & ADC_SR_EOC) {
		handle_conversion();
		ADC2->SR &= ~ADC_SR_EOC;
	}
}



//! Engine encoder interrupt handler
void TIM3_IRQHandler() {
	if(TIM3->SR & TIM_SR_UIF){
		data->mileage++;
		TIM3->SR &= ~TIM_SR_UIF;
	}
}

//! Wheel encoder interrupt handler
void EXTI9_5_IRQHandler() {
	if( EXTI->PR & EXTI_PR_PR5 ) {
		data->mileage++;
		EXTI->PR |= EXTI_PR_PR5;
	}
}


//! Function implemented in USB library, not used
void SetOutReport(void) {}


#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
