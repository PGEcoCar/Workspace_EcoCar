/*
 * gpio.c
 *
 *  Created on: 5 kwi 2016
 *      Author: Pawel
 */

#include "gpio.h"

//! GPIO init
/*!
 * Return values of switches during startup
 *
 * GPIO init
 * GPIOA PIN0 Analog- current measurement
 * GPIOA PIN1 Analog- voltage measurement
 * GPIOA PIN2 Analog- voltage measurement
 * GPIOA PIN3 Analog- voltage measurement
 * GPIOA PIN8 Alternate function output Push-pull PWM Timer 1 HIN high transistor signal
 * GPIOA PIN15 Input switch 1
 * GPIOB PIN1 USART3 Direction
 * GPIOB PIN3 Input switch 2
 * GPIOB PIN5 Input interrupt encoder timer
 * GPIOB PIN6 Input floating (default) Timer 4 Encoder input
 * GPIOB PIN7 Input floating (default) Timer 4 Encoder input
 * GPIOB PIN10 USART3 TX
 * GPIOB PIN11 USART3 RX
 * GPIOB PIN13 Alternate function output Push-pull PWM Timer 1 LIN low transistor signal
 * GPIOC PIN13 out LED
 */

uint8_t gpio_init() {
	uint8_t status = 0;
	//! Enable GPIO clocks
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	//! Release Jtag pins
	AFIO->MAPR |= AFIO_MAPR_SWJ_CFG_1;

	//! PA0 & PA1 analog
	GPIOA->CRL &= ~GPIO_CRL_CNF0;
	GPIOA->CRL &= ~GPIO_CRL_CNF1;
	GPIOA->CRL &= ~GPIO_CRL_CNF2;
	GPIOA->CRL &= ~GPIO_CRL_CNF3;

	//! PA8 AF timer1
	GPIOA->CRH &= ~( GPIO_CRH_CNF8);
	GPIOA->CRH |= GPIO_CRH_CNF8_1 | GPIO_CRH_MODE8;

	//! PA15 USB switch input pullup
	GPIOA->CRH &= ~GPIO_CRH_CNF15;
	GPIOA->CRH |= GPIO_CRH_CNF15_1;
	GPIOA->ODR |= GPIO_ODR_ODR15;

	//! PB1 Usart3 direction as output push pull
	/*!
	 * 1- transmit
	 * 0- receive
	 */
	GPIOB->CRL |= GPIO_CRL_MODE1_1;
	GPIOB->CRL &= ~GPIO_CRL_CNF1;
	GPIOB->ODR |= GPIO_ODR_ODR1;

	//! PB3 Encoder switch input pullup
	/*!
	 * 1- engine encoder interface
	 * 0- wheel encoder interface
	 */
	GPIOB->CRL &= ~GPIO_CRL_CNF3;
	GPIOB->CRL |= GPIO_CRL_CNF3_1;
	GPIOB->ODR |= GPIO_ODR_ODR3;

	//! PB6 PB7 input floating default value

	//! PB10 Usart3 TX
	GPIOB->CRH &= ~GPIO_CRH_CNF10;
	GPIOB->CRH |= GPIO_CRH_CNF10_1 | GPIO_CRH_MODE10;
	//! PB11 Usart3 RX input pull-up
	GPIOB->CRH &= ~GPIO_CRH_CNF11;
	GPIOB->CRH |= GPIO_CRH_CNF11_1;
	GPIOB->ODR |= GPIO_ODR_ODR11;

	//!PB13 AF timer1
	GPIOB->CRH &= ~( GPIO_CRH_CNF13);
	GPIOB->CRH |= GPIO_CRH_CNF13_1 | GPIO_CRH_MODE13;

	//! PC13 output
	GPIOC->CRH |= GPIO_CRH_MODE13_1;
	GPIOC->ODR |= GPIO_ODR_ODR13;

	//! Check the switch line and turn pullups off (0.7mA less,yay!)
	if( GPIOA->IDR & GPIO_IDR_IDR15 ) {
		status |= USB_ON;
	}
	if( GPIOB->IDR & GPIO_IDR_IDR3 ) {
		status |= ENCODER_ON;
		AFIO->MAPR |= AFIO_MAPR_TIM3_REMAP_1;
		GPIOB->CRL &= ~GPIO_CRL_CNF5;
		GPIOB->CRL |= GPIO_CRL_CNF5_1;
	}
	else{
		GPIOB->CRL &= ~GPIO_CRL_CNF5;
		GPIOB->CRL |= GPIO_CRL_CNF5_1;

		AFIO->EXTICR[1] |= AFIO_EXTICR2_EXTI5_PB;
		EXTI->IMR |= EXTI_IMR_MR5;
		EXTI->RTSR |= EXTI_RTSR_TR5;
		NVIC_EnableIRQ(EXTI9_5_IRQn);
	}
	GPIOA->ODR &= ~GPIO_ODR_ODR15;
	GPIOB->ODR &= ~GPIO_ODR_ODR3;

	return status;
}


