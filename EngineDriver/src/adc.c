/*
 * adc.c
 *
 *  Created on: 16 kwi 2016
 *      Author: Pawel
 */

#include "adc.h"


//! ADC1 init function
/*!
 * High speed ADC dedicated to current measurement
 * Triggered by TIM2, samples copied by DMA to adc_buffer
 * Circular mode, analog watchdog for upper and lower voltage
 * Sample rate and dma buffer length defined in adc.h
 */
void current_adc_init()
{
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	RCC->CFGR |= RCC_CFGR_ADCPRE_1;

	DMA1_Channel1->CCR = 0;
	DMA1_Channel1->CPAR = ( uint32_t ) &ADC1->DR;
	DMA1_Channel1->CMAR =(uint32_t) adc_buffer;
	DMA1_Channel1->CNDTR = ADC_BUFF_LEN;

	DMA1_Channel1->CCR |=     DMA_CCR1_CIRC		|
							  DMA_CCR1_MSIZE_0 	|
							  DMA_CCR1_PSIZE_0	|
			  	  	  	  	  DMA_CCR1_PL_1 	|
						  	  DMA_CCR1_MINC 	|
							  DMA_CCR1_TCIE 	|
							  DMA_CCR1_EN;

	ADC1->CR1 |= ADC_CR1_AWDIE;
	ADC1->LTR = 0xffff;
	//ADC1->HTR = 0xffff;
	ADC1->CR2 |= ADC_CR2_ADON		|
				 ADC_CR2_SWSTART	|
				 ADC_CR2_EXTTRIG	|
				 ADC_CR2_EXTSEL_0	|
				 ADC_CR2_EXTSEL_1	|
				 ADC_CR2_DMA;
	ADC1->SMPR2 |= ADC_SMPR2_SMP0_0;

	//TIM2->PSC = 1; //1
	TIM2->ARR = ((SystemCoreClock>>1)/SAMPLE_RATE )-1;
	TIM2->CCR2 = (TIM2->ARR)>>1;
	TIM2->CCER |= TIM_CCER_CC2E;
	TIM2->CCMR1 |= TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2;
	TIM2->CCMR1 |= TIM_CCMR1_OC2PE;
	TIM2->EGR |= TIM_EGR_CC2G;
	TIM2->CR1 |= TIM_CR1_CEN;

	counter = 1;

	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
	NVIC_EnableIRQ( ADC1_2_IRQn );
	ADC1->CR2 |= ADC_CR2_CAL;
}

//! ADC2 init function
/*!
 * Dedicated to slow and accurate measurement
 * Three conversions in the following order: channel 3, 2, 1
 */
void slow_adc_init()
{
	RCC->APB2ENR |= RCC_APB2ENR_ADC2EN;
	ADC2->CR1 |= 	ADC_CR1_EOCIE | ADC_CR1_DISCEN |
					ADC_CR1_DISCNUM_1 | ADC_CR1_DISCNUM_0;
	ADC2->CR2 |=	ADC_CR2_EXTSEL | ADC_CR2_EXTTRIG;
	//! Conversion1 channel 1
	ADC2->SQR3 |= ADC_SQR3_SQ1_1 | ADC_SQR3_SQ1_0;
	ADC2->SQR3 |= ADC_SQR3_SQ2_1;
	ADC2->SQR3 |= ADC_SQR3_SQ3_0;
	ADC2->SQR1 |= ADC_SQR1_L_1;

	ADC2->SMPR2 |= 	ADC_SMPR2_SMP1 |
					ADC_SMPR2_SMP2 |
					ADC_SMPR2_SMP3;
	ADC2->CR2 |= ADC_CR2_ADON;
	ADC2->CR2 |= ADC_CR2_CAL;
}


//! DMA handler
/*!
 * First sample accumulated to prevent overwriting it by DMA
 * The remaining samples accumulated in the main loop by average() function
 */
void DMA1_Channel1_IRQHandler() {
	acc+= adc_buffer[0];
	samples_ready = 1;
    DMA1->IFCR |= DMA_IFCR_CTCIF1;
}


//! Set maximum value of current
void set_upper_limit(uint16_t val) {
	ADC1->HTR = val+MARGIN;
	ADC1->LTR = 0;
}

//! Set minimum value of current
void set_lower_limit(uint16_t val) {
	ADC1->LTR = val-MARGIN;
	ADC1->HTR = 0xffff;
}

void average() {
	if(samples_ready) {
		for(uint16_t i=1; i<ADC_BUFF_LEN;i++) acc+=adc_buffer[i];
		counter+= ADC_BUFF_LEN;
		samples_ready = 0;
	}
}

void start_slow_conversion(uint16_t *pointer){
	ADC2->CR2 |= ADC_CR2_SWSTART;
	p = pointer;
}

void handle_conversion() {
	*p = ADC2->DR;
	p++;
}
