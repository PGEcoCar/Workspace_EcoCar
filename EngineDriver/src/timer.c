/*
 * timer.c
 *
 *  Created on: 16 kwi 2016
 *      Author: Pawel
 */

#include "timer.h"

//! PWM driver timer init function
void pwm_timer_init() {
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;

	TIM1->PSC = 0;
	TIM1->ARR = TIM_ARR;
	TIM1->CCR1 = TIM_ARR;
	TIM1->CCMR1 |= TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2;
	TIM1->BDTR |=  TIM_BDTR_MOE | TIM_BDTR_DTG_4 ;
	TIM1->CR1 |= TIM_CR1_CEN;
}


//! Encoder timer init function
void encoder_timer_init() {
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	TIM4->PSC = 0x00;
	TIM4->ARR = 0xFFFF;
	TIM4->SMCR |= TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;

	TIM4->CCMR1 |= TIM_CCMR1_CC1S_0;
	TIM4->CCMR1 |= TIM_CCMR1_CC2S_0;
	TIM4->CCER |= TIM_CCER_CC1P;
	TIM4->CR1 |= TIM_CR1_CEN;
}


//! Encoder2 timer init function
void mileage_timer_init() {
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;   //TIM2 enable
	TIM3->ARR = 18;		// 10s
	TIM3->CCMR1 |= TIM_CCMR1_CC2S_0;
	TIM3->SMCR |= TIM_SMCR_TS_2 | TIM_SMCR_TS_1 | TIM_SMCR_SMS;
	TIM3->DIER |= TIM_DIER_UIE;
	TIM3->CR1 |= TIM_CR1_CEN;
	TIM3->SR &= ~(0x1F);
	NVIC_EnableIRQ(TIM3_IRQn);
}


//! Get the encoder timer value
uint16_t get_encoder_val(uint16_t *val)
{
	*val = TIM4->CNT;
	TIM4->CNT = 0;
	return *val;
}


void set_high() {
	TIM1->CCR1 = TIM_ARR;
	TIM1->CNT = TIM_ARR;
}

//! Turn on low transistor
void set_low() {
	TIM1->CCR1 = 1;
	TIM1->CNT = TIM_ARR;
}

//! Turn on PWM and analog watchdog
void pwm_on() {
	TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE;
	ADC1->CR1 |= ADC_CR1_AWDEN;
}

//! Turn off PWM and analog watchdog
void pwm_off() {
	TIM1->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC1NE);
	ADC1->CR1 &= ~ADC_CR1_AWDEN;
}


