/*----------------------------------------------------------------------------
 *      U S B  -  K e r n e l
 *----------------------------------------------------------------------------
 *      Name:    USBDESC.C
 *      Purpose: USB Descriptors
 *      Version: V1.10
 *----------------------------------------------------------------------------
 *      This file is part of the uVision/ARM development tools.
 *      This software may only be used under the terms of a valid, current,
 *      end user licence from KEIL for a compatible version of KEIL software
 *      development tools. Nothing else gives you the right to use it.
 *
 *      Copyright (c) 2005-2007 Keil Software.
 *---------------------------------------------------------------------------*/

#include "type.h"

#include "usb.h"
#include "hid.h"
#include "usbcfg.h"
#include "usbdesc.h"

/* HID Report Descriptor */
const BYTE HID_ReportDescriptor[] = {

	HID_UsagePageVendor(0x00),
	HID_Usage(0x01),
	HID_Collection(HID_Application),
		HID_UsagePage(HID_USAGE_PAGE_UNDEFINED), //edit
		HID_UsageMin(1),
		HID_UsageMax(8),
		HID_LogicalMin(0),
		0x26,0xff,0x00,//HID_LogicalMax(127),
		HID_ReportCount(16),
		HID_ReportSize(8),
		HID_Input(HID_Data | HID_Variable | HID_Absolute),
		/*HID_ReportCount(1),
		HID_ReportSize(6),
		HID_Input(HID_Constant),
		HID_UsagePage(HID_USAGE_PAGE_LED),
		HID_Usage(
		HID_USAGE_LED_GENERIC_INDICATOR),
		HID_LogicalMin(0),
		HID_LogicalMax(1), HID_ReportCount(8), HID_ReportSize(1),
		HID_Output(HID_Data | HID_Variable | HID_Absolute),*/
	HID_EndCollection
};

//edit
/*  HID_Usage(0x01),
 HID_LogicalMin(0),
 0x26,0xff,0x00, //logical absoulute max
 HID_ReportSize(8),
 HID_ReportCount(1),

 HID_Output(HID_Data | HID_Variable | HID_Absolute),
 HID_EndCollection*/

const WORD HID_ReportDescSize = sizeof(HID_ReportDescriptor);



/* USB Standard Device Descriptor */
const BYTE USB_DeviceDescriptor[] = {
	USB_DEVICE_DESC_SIZE, 					/* bLength */
	USB_DEVICE_DESCRIPTOR_TYPE, 			/* bDescriptorType */
	WBVAL(0x0110), 			/* 1.10 */		/* bcdUSB */
	0x00, 									/* bDeviceClass */
	0x00, 									/* bDeviceSubClass */
	0x00, 									/* bDeviceProtocol */
	USB_MAX_PACKET0, 						/* bMaxPacketSize0 */
	WBVAL(0x16c0), 							/* idVendor */
	WBVAL(0x05E5), 							/* idProduct */
	WBVAL(0x0100), 			/* 1.00 */		/* bcdDevice */
	0x04, 									/* iManufacturer */
	0x1C,									/* iProduct */
	0x3C, 									/* iSerialNumber */
	0x01 									/* bNumConfigurations */
};



/* USB Configuration Descriptor */
/*   All Descriptors (Configuration, Interface, Endpoint, Class, Vendor */
const BYTE USB_ConfigDescriptor[] = {

	/* Configuration 1 */
	9, 					       				/* bLength */
	USB_CONFIGURATION_DESCRIPTOR_TYPE, 		/* bDescriptorType */
	WBVAL(0x22),                            	/* wTotalLength */
	0x01, 								 	/* bNumInterfaces */
	0x01,									/* bConfigurationValue */
	0x00,									/* iConfiguration */
	USB_CONFIG_BUS_POWERED, 				/* bmAttributes */
	USB_CONFIG_POWER_MA(100), 				/* bMaxPower */

	/* Interface 0, Alternate Setting 0, HID Class */
	9,							          	/* bLength */
	USB_INTERFACE_DESCRIPTOR_TYPE, 			/* bDescriptorType */
	0x00, 									/* bInterfaceNumber */
	0x00, 									/* bAlternateSetting */
	0x01, 									/* bNumEndpoints */
	USB_DEVICE_CLASS_HUMAN_INTERFACE, 		/* bInterfaceClass */
	HID_SUBCLASS_NONE, 						/* bInterfaceSubClass */
	HID_PROTOCOL_NONE, 						/* bInterfaceProtocol */
	0x50, 		                            /* iInterface */

	/* HID Class Descriptor */
	/* HID_DESC_OFFSET = 0x0012 */
	9, 					                    /* bLength */
	HID_HID_DESCRIPTOR_TYPE, 				/* bDescriptorType */
	WBVAL(0x0100), 							/* 1.00 *//* bcdHID */
	0x00, 									/* bCountryCode */
	0x01,									/* bNumDescriptors */
	HID_REPORT_DESCRIPTOR_TYPE, 			/* bDescriptorType */
	WBVAL(HID_REPORT_DESC_SIZE), 			/* wDescriptorLength */

	/* Endpoint, HID Interrupt In */
	7,							            /* bLength */
	USB_ENDPOINT_DESCRIPTOR_TYPE, 			/* bDescriptorType */
	USB_ENDPOINT_IN(1), 					/* bEndpointAddress */
	USB_ENDPOINT_TYPE_INTERRUPT,			/* bmAttributes */
	WBVAL(0x0010),							/* wMaxPacketSize */
	0xff, /* 32ms */						/* bInterval */
											/* Terminator */
	0										/* bLength */
};

/* USB String Descriptor (optional) */
const BYTE USB_StringDescriptor[] = {

	/* Index 0x00: LANGID Codes */
	0x04, 								/* bLength */
	USB_STRING_DESCRIPTOR_TYPE, 		/* bDescriptorType */
	WBVAL(0x0409), 	/* US English */	/* wLANGID */

	/* Index 0x04: Manufacturer */
	0x18,                             	/* bLength */
	USB_STRING_DESCRIPTOR_TYPE, 		/* bDescriptorType */
	'E', 0, 'c', 0, 'o', 0, 'C', 0, 'a', 0, 'r', 0, ' ', 0, 's', 0, 'o', 0,
	'f', 0, 't', 0,

	/* Index 0x20: Product */
	0x20, 								/* bLength */
	USB_STRING_DESCRIPTOR_TYPE, 		/* bDescriptorType */
	'E', 0, 'c', 0, 'o', 0, 'C', 0, 'a', 0, 'r', 0, ' ', 0, 'L', 0, 'i', 0,
	'v', 0, 'e', 0, 'V', 0, 'i', 0, 'e', 0, 'w', 0,

	/* Index 0x44: Serial Number */
	0x14,                            	/* bLength */
	USB_STRING_DESCRIPTOR_TYPE, 		/* bDescriptorType */
	'v', 0, 'e', 0, 'r', 0, '.', 0, ' ', 0, '1', 0, '.', 0, '0', 0, '0', 0,

	/* Index 0x5E: Interface 0, Alternate Setting 0 */
	0x08, 								/* bLength */
	USB_STRING_DESCRIPTOR_TYPE, 		/* bDescriptorType */
	'H', 0, 'I', 0, 'D', 0
};


