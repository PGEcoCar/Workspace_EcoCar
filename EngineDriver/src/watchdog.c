/*
 * watchdog.c
 *
 *  Created on: 17 kwi 2016
 *      Author: Pawel
 */

#include "watchdog.h"

//! Watchdog init funciton
//! Reset set to 110ms
void watchdog_init() {

	IWDG->KR |= WDG_CONFIG;
	IWDG->RLR |=  0x44C;
	IWDG->KR |= WDG_START;
}

