
#include "usart.h"

//! USART3 init
/*!
 * USART3 init
 * Baud rate defined in usart.h
 * DMA pointing at 'buffer' in circular mode
 * Recieving not implemented yet
 */
void usart3_init(void) {
	RCC->APB1ENR |=  RCC_APB1ENR_USART3EN;
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;

	uint32_t tmpreg = 0x00, apbclock = 0x00;
	uint32_t integerdivider = 0x00;
	uint32_t fractionaldivider = 0x00;
	apbclock = SystemCoreClock/2;

	USART3->CR1 |= USART_CR1_UE;
	USART3->CR3 |= USART_CR3_DMAT;

	integerdivider = (25*apbclock)/(4*(BAUD));
	tmpreg = (integerdivider / 100) << 4;
	fractionaldivider = integerdivider - (100 * (tmpreg >> 4));
	tmpreg |= ((((fractionaldivider * 16) ) / 100)) & ((uint8_t)0x0F);

	USART3->BRR = (uint16_t)tmpreg;
	USART3->CR1 |= USART_CR1_TE;

	DMA1_Channel2->CCR = 0;
	DMA1_Channel2->CPAR = ( uint32_t ) &USART3->DR;
	DMA1_Channel2->CMAR = (uint32_t) out_buffer;
	DMA1_Channel2->CNDTR = OUT_BUFF_LEN;

	DMA1_Channel2->CCR |=     DMA_CCR2_CIRC |
			  	  	  	  	  DMA_CCR2_PL_0 |
							  DMA_CCR2_DIR  |
						  	  DMA_CCR2_MINC;
}


void usart3_send() {
	DMA1->IFCR |= DMA_IFCR_CTCIF2;
	DMA1_Channel2->CCR |= DMA_CCR2_EN;
}

