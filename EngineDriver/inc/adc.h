/*
 * adc.h
 *
 *  Created on: 16 kwi 2016
 *      Author: Pawel
 */

#ifndef ADC_H_
#define ADC_H_

#include "stm32f10x.h"

//! Size of ADC buffer
#define ADC_BUFF_LEN	500
//! Sample rate of high speed ADC
#define SAMPLE_RATE 500000
//! Hysteresis margin. Calculation: Vdd*MARGIN/( 4095* Transducer_sensitivity)
#define MARGIN 10

//! ADC buffer
uint16_t adc_buffer[ADC_BUFF_LEN];
//! Start averaging flag
uint8_t samples_ready;
//! Averaging counter
uint16_t counter;
//! Samples accumulator
uint32_t acc;
//! Slow conversion destination pointer
uint16_t *p;


void current_adc_init();
void slow_adc_init();


//! Set maximum value of current
void set_upper_limit(uint16_t val);

//! Set minimum value of current
void set_lower_limit(uint16_t val);

//! Accumulate samples gathered by DMA
void average();

//! Start slow conversion, pointer to the destination of sample
void start_slow_conversion(uint16_t *pointer);

//! Handle slow conversion, increment destination pointer
void handle_conversion();

#endif /* ADC_H_ */
