/*
 * watchdog.h
 *
 *  Created on: 17 kwi 2016
 *      Author: Pawel
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include "stm32f10x.h"

#define WDG_START 0xCCCC
#define WDG_CONFIG 0x5555
#define WDG_RELOAD 0xAAAA
#define wdg_reset IWDG->KR |= WDG_RELOAD

void watchdog_init();

#endif /* WATCHDOG_H_ */
