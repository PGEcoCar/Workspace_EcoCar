/*
 * timer.h
 *
 *  Created on: 16 kwi 2016
 *      Author: Pawel
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "stm32f10x.h"

//! Prevent driver turn-off
#define TIM_ARR		60000//1800

void pwm_timer_init();
void encoder_timer_init();
void mileage_timer_init();

uint16_t get_encoder_val(uint16_t *val);

//! Turn on high transistor
void set_high();

//! Turn on low transistor
void set_low();

//! Turn on PWM and analog watchdog
void pwm_on();

//! Turn off PWM and analog watchdog
void pwm_off();



#endif /* TIMER_H_ */
