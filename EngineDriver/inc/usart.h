#ifndef USART_H_
#define USART_H_

#include "stm32f10x.h"

#define BAUD 230400
#define OUT_BUFF_LEN sizeof(data_struct)

//! Data struct specified to the mesurements
#pragma pack(1)
typedef struct {
	uint16_t time;
	uint16_t motor_current;
	uint16_t voltage;
	uint16_t adc_1;
	uint16_t adc_2;
	uint16_t mileage;
	uint16_t speed;
	uint16_t rpm;
} data_struct;

//! Output buffer, byte representation of data_struct, share same pointer in main
volatile uint8_t out_buffer[OUT_BUFF_LEN];

void usart3_init(void);

//! Send output buffer
void usart3_send();

#endif
