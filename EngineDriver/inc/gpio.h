#ifndef GPIO_H_
#define GPIO_H_

#include "stm32f10x.h"

#define USB_ON 0x01
#define KERS_ON 0x02
#define ENCODER_ON 0x03

uint8_t gpio_init();

#endif
